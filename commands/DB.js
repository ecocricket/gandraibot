const nedb    = require('nedb');
const Promise = require('promise');
const path    = require('path');
var db; //main database

var opened = false;

class Database
{
    constructor()
    {   //----------------init----------//
        if(!opened)
        {
            var pathTo = path.dirname(__dirname)+'/DB/tweets.db';
            db = new nedb({filename:pathTo});
            opened = true;
        }
        
        db.loadDatabase(err=>
        {
            if(err) throw err;     
        });
        /**
         * delete if there is more than 10 tweets and leave only the newest
         */
        this.DeleteOldRecords({tweet:"pubg"}); //default 10
        
    }

    Insert(obj)
    {
        //var s_obj = JSON.stringify(obj);
        return new Promise((success, error)=>
        {
             db.insert(obj,(err, newDoc) =>
            {
                if(err) error(err);
                success(newDoc);
            });
        });
       
    }

    Update(findData,updateData)
    {
        return new Promise((success, error)=>
        {
            db.update(findData,updateData,(err,replacedId)=>
            {
                if(err) error(err);
                success(replacedId);
            });
        })
        
    }

    Delete(query,multi=false)
    {   
        return new Promise((success, error)=>{
            db.remove(query,{multi:multi},(err,removedId)=>
            {
                if(err) error(err);
                success(removedId);
            });
        });
        
    }

    Query(query)
    {
        return new Promise((success,error)=>
        {
             db.find(query,(err,docs) =>
             {
                if(err) error(err);
                success(docs);
             });
        });
       
    }

    Count(query)
    {
        return new Promise((success, error)=>
        {
            db.count(query,(err,count)=>
            {
                if(err) error(err);
                success(count);
            });
        });
    }

    /**
     * for deleting old records;
     */
    DeleteOldRecords(query,olderThan = 10)
    {
        //this.Delete(query,{multi:true});
        this.Count(query).then((count)=>
        {

            if(count>olderThan)
            {
               this.Query(query).then((res)=>
               {
                    //console.log(res);
                    var toDelete = [];
                    var minValues= [];
                    for(var i = 0;i<count;i++)
                    {
                        toDelete.push(res[i].time);
                    }

                    for(var i = 10;i<count;i++)
                    {
                        var oldest = Math.min.apply(Math,toDelete)
                        minValues.push(oldest);
                        toDelete.splice(toDelete.indexOf(oldest),1);
                    }

                    //console.log(minValues);
                    for(var i = 0;i<minValues.length;i++)
                    {
                        this.Delete({tweet:"pubg",time:minValues[i]});
                    }
                   
                    //this.Delete({tweet:"pubg"},{multi:true});
               });
            }
        }).catch(error=>{throw error;});
    }

}

module.exports = Database;