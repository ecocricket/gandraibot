const API      = require("twitter");
const fs       = require("fs");
const Promise  = require("promise");
const Messages = require("../../discord/messages");
const DB       = require("../../DB");
//-------------------------------------------//
const messages = new Messages();
const db       = new DB();

//------------------------------------------------------//
// main logic to get pubg news from twitter
//------------------------------------------------------//
var config; //json for api
var answer; //channel to respond
var limit = 20 // limit of tweets per command
var This;
class Pubg
{

    constructor()
    {
        this.GetJSON();
        This = this;
        //db.Delete({tweet:'pubg',time:new Date("Thu Jun 01 12:19:24 +0000 2017").getTime()});
    }
    HandlePubgRequest(args,answerTo)
    {
        answer = answerTo;
        var req = args.constructor === Array ? args[0] : args;

        
        if(req=='auto')
        {
            //havent implemented that yet
            this.Stream();
            return;
        }

        if(req=='stop')
        {
            if(!this._stream )
            {
                messages.Send(answerTo,'stream is already stopped...');
                return;
            }
            clearInterval(this._stream);
            delete this._stream;
            messages.Send(answerTo,'stream stopped');
            return;
        }

        req = parseInt(req);

        req > limit ? req = limit : req;

        if(Number.isInteger(req))
        {
            var tweets;
            this.GetLatest(req).then((rez)=>
            {
                var tweets = rez[0];
                var howMuch= rez[1];
                var tweets_to_send = [];
                //WriteToFile(tweets);
                /**
                 * create custom messages
                 */
                for(var i = 0; i<tweets.length;i++)
                {
                    /**
                     * testing adding tweets to database
                     */
                    // db.Insert({tweet:"pubg",content:tweets[i],time:new Date(tweets[i].created_at).getTime()}).then((res)=>
                    // {
                     
                    // }).catch(err=>
                    // {
                    //     if(err) throw err;
                    // });

                    tweets_to_send.push(this.CreateEmbed(tweets[i],i+1,howMuch));
                    
                }
                /**
                 * 
                 * write these custom messages to the discord
                 */
                for(var i = 0; i<tweets.length;i++)
                {   
                    messages.Send(answerTo,'',{embed:tweets_to_send[i]});
                    //if tweet contains video, display it in discord
                    if(tweets[i].extended_entities != undefined)
                    {
                        messages.Send(answerTo,tweets[i].extended_entities.media[0].expanded_url);
                    }
                }
                
            }).catch((error)=>
            {
                console.log(json.stringify(error));  
            });
        }

    return;

    }
    GetLatest(howMuch)
    {
        if(!config) throw "Config is not initialised yet";
        //var uri = encodeURIComponent("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=PUBATTLEGROUNDS&count=4");
         
        return new Promise((succes,error)=>
        {
            config.get('statuses/user_timeline', 
            {
                screen_name:"PUBATTLEGROUNDS",
                count:howMuch+15,//in case there are reply among tweets
                //since_id:"867041670101180415",
                //max_id:"867041670101180417",
                exclude_replies:true,
                

            },(err,tweets,response) =>
            {
                
                if(err) error(err);
                tweets = tweets.slice(0,howMuch);
                succes([tweets,howMuch]);
                //console.log("\nTWEETS:\n"+JSON.stringify(tweets, null, "\t") );
                //this.writeToFile(tweets);
                // console.log("\nResponse:\n"+JSON.stringify(response,null,"\t"));
            });
            //  stream.on('data', function(event) {
            //     console.log(event && event.text);
            //  });
        })
         
    }
    Stream()
    {
        if(!config) throw "Config is not initialised yet";

        this._stream = setInterval(function()
        {
             config.get('statuses/user_timeline', 
            {
                screen_name:"PUBATTLEGROUNDS",
                count:6,
                exclude_replies:true
                
                

            },(err,tweets,response) =>
            {
                if(err) throw(err);
               // var tmp = new Pubg();
                This.CheckPastTweets(tweets);                            
                
                //messages.Send(answer,'',{embed:this.CreateEmbed(tweet)});
                
            });
        },300000); //5mins
       

       messages.Send(answer,'Stream started'); 
    }
   
    GetJSON()
    {
        return new Promise((succes,error)=>
        {
            fs.readFile('./commands/twitter/tokens.json',(error,data) =>
            {
                if(error) throw error && error(error);
                config = new API(JSON.parse(data)); //generating config file using twitter module
                succes();
                //this.get();
            });
        });
        
    }

    CreateEmbed(Tweet,curr=1,max=1)
    {
        return {
  
            title: `${Tweet.user.screen_name} ${curr}/${max}`,
            color:3394815,
            footer:{text:`Created ${Tweet.created_at}` },
            thumbnail:{url:Tweet.user.profile_image_url_https},
            fields: [{
                    name: 'Content',
                    value: Tweet.text.replace(/&amp;/g, "&"),
                    inline: false
            }, {
                    name: 'Link',
                    value: `https://twitter.com/PUBATTLEGROUNDS/status/${Tweet.id_str}`,
                    inline: false
            }]

        }
    }

    WriteToFile(tweets)
    {
         fs.writeFile('./tweets.json',JSON.stringify(tweets, null, "\t"),(error,data) =>
            {
                if(error) throw error
                
            });
    }

    CheckPastTweets(tweets)
    {
        //
         var tmp=0;
         var promisesCount = [];
         //var PromisesSend  = [];
        // for(var i = 0; i<tweets.length;i++)
        // {
        //     for(var j = 0; j<db_tweets.length;j++)
        //     {
        //         //if tweets doesnt match the ones in the database
        //         if(tweets[i].created_at!=db_tweets[j].created_at)
        //         {
        //             //if last tweet didn't match, insert into database and
        //             if(j==(db_tweets.length-1))
        //             { 
        //                 db.Insert({tweet:'pubg',content:m_tweets[i],time:new Date(m_tweets[i].created_at).getTime()});
        //                 messages.Send(answer,'',{embed:this.CreateEmbed(m_tweets[i])}); 
        //                 tmp=1;        
        //             }
        //         }
        //         else
        //         {    
        //             break;
        //         }
        //     }
        // }

        /**
         * another faster version depending on nedb
         */

        for(var i=0;i<tweets.length;i++)
        {
            promisesCount.push(db.Count({tweet:"pubg",time:new Date(tweets[i].created_at).getTime()}));
            // db.Count({tweet:"pubg",time:new Date(tweets[i].created_at).getTime()}).then((res)=>
            // {
            //    if(res<1)
            //    {
            //     messages.Send(answer,'',{embed:this.CreateEmbed(tweets[i])}).then((res)=>
            //     {
            //          db.Insert({tweet:'pubg',content:tweets[i],time:new Date(tweets[i].created_at).getTime()});
            //     }).catch(err=>{if(err) throw err}); 
                
            //     tmp=1;       
            //    }
            // }).catch(err=>
            // {
            //     if(err) throw err;
            // });
        }

        Promise.all(promisesCount).then((res)=>
        {
            for(var i=0;i<res.length;i++)
            {
                if(res[i]<1)
                {
                    messages.Send(answer,'',{embed:this.CreateEmbed(tweets[i])});
                    db.Insert({tweet:'pubg',content:tweets[i],time:new Date(tweets[i].created_at).getTime()});

                }
            }
            
        });

        // Promise.all(PromisesSend).then((res)=>
        // {
        //     console.log(res);
        // });
        //return tmp ? true : false;
    }
    
}

module.exports = Pubg;

